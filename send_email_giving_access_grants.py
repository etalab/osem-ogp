#! /usr/bin/env python3


import argparse
from collections import OrderedDict
import email.header
import logging
import os
import re
import smtplib
import sys
import time

import mako.lookup
import mysql.connector


app_dir = os.path.dirname(os.path.abspath(__file__))
app_name = os.path.splitext(os.path.basename(__file__))[0]
cache_dir = os.path.join(app_dir, 'cache')
line_re = re.compile(u"""(?P<indent>\s*)(?P<header>([-*]|[\d]+\.)\s*|)(?P<content>[^\s].*)$""")

config = {
    'user': 'root',
    'password': '',
    'host': '127.0.0.1',
    'database': 'osem_prod',
    'raise_on_warnings': True,
    # 'use_pure': False,
    }


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--count', default = -1, help = 'max number of emails to send', type = int)
    parser.add_argument('-f', '--from-email', help = '"From" email address to use', required = True)
    parser.add_argument('-G', '--go', action = 'store_true', default = False,
        help = "send emails to the real email addresses")
    parser.add_argument('-m', '--model', help = 'name of template to use for emails', required = True)
    parser.add_argument('-r', '--resume', help = 'resume an interrupted spam campaign after given email')
    parser.add_argument('-s', '--send', action = 'store_true', default = False,
        help = "send emails (by default, no email is sent)")
    parser.add_argument('-t', '--to-email', nargs = '+', help = '"To" email address to use')
    parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = "increase output verbosity")
    args = parser.parse_args()

    log = logging.getLogger(app_name)
    logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING, stream = sys.stdout)

    templates_lookup = mako.lookup.TemplateLookup(
        directories = [os.path.join(app_dir, 'email-templates')],
        input_encoding = 'utf-8',
        module_directory = os.path.join(cache_dir, 'templates'),
        )
    template = templates_lookup.get_template(args.model)

    connection = mysql.connector.connect(**config)
    cursor = connection.cursor()

    columns_name = [
        'users.id',
        'registrations.id',
        'users.email',
        'registrations.confirmation_date',
        'registrations.first_name',
        'registrations.last_name',
        'registrations.valid_23',  # Civil Society Morning - December 7
        'registrations.valid_24',  # Opening plenary - December 7
        'registrations.valid_25',  # Panels and workshops - December 8
        'registrations.valid_26',  # Democracy Night - December 8
        'registrations.valid_27',  # Toolbox hackathon - December 8 and 9
        'registrations.valid_28',  # Closing plenary - December 9
        'registrations.valid_46',  # Subnational sessions - December 9
        'registrations.valid_47',  # Open Parliament - December 8 (morning)
        'registrations.valid_48',  # Panels and workshops - December 9
        'registrations.valid_49',  # Open Parliament - December 8 (afternoon)
        ]
    # Note: 27 below is the id of the answer for the question: "Toolbox hackathon - December 8 and 9".
    query = ("""\
        SELECT {columns}
        FROM registrations
        INNER JOIN users
        ON registrations.user_id = users.id
        WHERE registrations.conference_id = 1
        """.format(
            columns = ', '.join(columns_name)
            ))
    cursor.execute(query)

    entries = [
        OrderedDict(zip(columns_name, row))
        for row in cursor
        ]

    cursor.close()
    connection.close()

    email_count = 0
    from_email = args.from_email
    skip = bool(args.resume)
    try:
        for entry in entries:
            to_email = entry['users.email']
            if skip:
                if args.resume == to_email:
                    # Email found. Resuming after this recipient...
                    skip = False
                continue
            if (not (entry['registrations.valid_23'] or '').strip()  # Civil Society Morning - December 7
                    and not (entry['registrations.valid_24'] or '').strip()  # Opening plenary - December 7
                    and not (entry['registrations.valid_25'] or '').strip()  # Panels and workshops - December 8
                    and not (entry['registrations.valid_28'] or '').strip()  # Closing plenary - December 9
                    and not (entry['registrations.valid_46'] or '').strip()  # Subnational sessions - December 9
                    and not (entry['registrations.valid_47'] or '').strip()  # Open Parliament - December 8 (morning)
                    and not (entry['registrations.valid_48'] or '').strip()  # Panels and workshops - December 9
                    and not (entry['registrations.valid_49'] or '').strip()  # Open Parliament - December 8 (afternoon)
                    ):
                continue

            full_name = ' '.join(
                fragment.strip()
                for fragment in (entry['registrations.first_name'], entry['registrations.last_name'])
                if fragment and fragment.strip()
                )
            msg = template.render_unicode(
                confirmation_date = entry['registrations.confirmation_date'],
                encoding = 'utf-8',
                first_name = entry['registrations.first_name'],
                from_email = from_email,
                full_name = full_name,
                last_name = entry['registrations.last_name'],
                qp = lambda s: to_quoted_printable(s, 'utf-8'),
                to_email = to_email,
                valid_23 = (entry['registrations.valid_23'] or '').strip(),  # Civil Society Morning - December 7
                valid_24 = (entry['registrations.valid_24'] or '').strip(),  # Opening plenary - December 7
                valid_25 = (entry['registrations.valid_25'] or '').strip(),  # Panels and workshops - December 8
                valid_26 = (entry['registrations.valid_26'] or '').strip(),  # Democracy Night - December 8
                valid_27 = (entry['registrations.valid_27'] or '').strip(),  # Toolbox hackathon - December 8 and 9
                valid_28 = (entry['registrations.valid_28'] or '').strip(),  # Closing plenary - December 9
                valid_46 = (entry['registrations.valid_46'] or '').strip(),  # Subnational sessions - December 9
                valid_47 = (entry['registrations.valid_47'] or '').strip(),  # Open Parliament - December 8 (morning)
                valid_48 = (entry['registrations.valid_48'] or '').strip(),  # Panels and workshops - December 9
                valid_49 = (entry['registrations.valid_49'] or '').strip(),  # Open Parliament - December 8 (afternoon)
                ).strip()
            # Rewrap message.
            msg_lines = []
            in_header = True
            for line in msg.splitlines():
                line = line.rstrip().replace(u' :', u' :').replace(u' [', u' [').replace(u'« ', u'« ').replace(
                    u' »', u' »')
                if not line:
                    in_header = False
                msg_lines.append(line)
            msg = u'\r\n'.join(msg_lines).replace(u' ', u' ').encode('utf-8')
            if args.send:
                server = smtplib.SMTP('localhost')
                if args.go:
                    try:
                        server.sendmail(from_email, [to_email], msg)
                    except smtplib.SMTPRecipientsRefused:
                        log.exception(u'Skipping email to {} <{}>, because an exception occurred:'.format(
                            full_name, to_email))
                        continue
                    log.info(u'An email was sent to {} <{}>'.format(full_name, to_email))
                else:
                    server.sendmail(from_email, args.to_email, msg)
                server.quit()
            else:
                log.info(u'An email will be sent to {} <{}>'.format(full_name, to_email))
            email_count += 1
            if args.count > 0 and email_count >= args.count:
                break
            if args.go:
                time.sleep(5)
    finally:
        print('{} emails sent.'.format(email_count))

    return 0


def to_quoted_printable(s, encoding):
    assert isinstance(s, str)
    quoted_words = []
    for word in s.split(' '):
        try:
            word = str(word)
        except UnicodeEncodeError:
            word = str(email.header.Header(word.encode(encoding), encoding))
        quoted_words.append(word)
    return ' '.join(quoted_words)


if __name__ == "__main__":
    sys.exit(main())
