#!/usr/bin/env python3

import sys, shutil, csv, os

COLUMNNAME = sys.argv[1]
ORIGIN = sys.argv[2]
DESTINATION = sys.argv[3]


def parseRow(row):
    user_id = row[0]
    writer.write("UPDATE registrations SET ")
    writer.write(COLUMNNAME)
    writer.write(" = '")
    writer.write("X")
    writer.write("' WHERE user_id = ")
    writer.write(user_id)
    writer.write(";\n")

writer = open(DESTINATION, "w", encoding = "UTF-8")
with open(ORIGIN, "r", encoding="UTF-8", newline='') as csvfile:
    csvreader = csv.reader(csvfile, delimiter=',')
    id_set = set()
    for row in csvreader:
        user_id = row[0]
        if user_id in id_set:
            print("doublon : " + user_id)
        else:
            id_set.add(user_id)
            parseRow(row)
