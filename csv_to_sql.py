#!/usr/bin/env python3

import sys, shutil, csv, os


ORIGIN_DIR = sys.argv[1]
DESTINATION_DIR = sys.argv[2]


def parseRow(row, columnname):
    user_id = row[0]
    writer.write("UPDATE registrations SET ")
    writer.write(columnname)
    writer.write(" = '")
    writer.write("X")
    writer.write("' WHERE user_id = ")
    writer.write(user_id)
    writer.write(";\n")

for filename in os.listdir(ORIGIN_DIR):
    if filename.endswith(".csv"):
        print(filename)
        destinationname = filename + ".sql"
        writer = open(DESTINATION_DIR + destinationname, "w", encoding = "UTF-8")
        with open(ORIGIN_DIR + filename, "r", encoding="UTF-8", newline='') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=',')
            first = True
            columnname = False
            id_set = set()
            for row in csvreader:
                if not first:
                    user_id = row[0]
                    if user_id in id_set:
                        print("doublon : " + user_id)
                    else:
                        id_set.add(user_id)
                        parseRow(row, columnname)
                else:
                    columnname = row[1]
                    print(columnname)
                    first = False