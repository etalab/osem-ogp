class OgpUploadCommercials < ActiveRecord::Migration
  def change
    add_column :commercials, :image_file_name, :string
    add_column :commercials, :image_content_type, :string
    add_column :commercials, :image_file_size, :integer
    add_column :commercials, :image_updated_at, :datetime
  end
end
