class OgpSummitSessionObjective < ActiveRecord::Migration
  def change
    add_column :events, :objective, :text
    add_column :event_types, :minimum_objective_length, :integer, null: false, default: 0
    add_column :event_types, :maximum_objective_length, :integer, null: false, default: 250
  end
end
