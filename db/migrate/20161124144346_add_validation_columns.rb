class AddValidationColumns < ActiveRecord::Migration
  def change
    add_column :registrations, :valid_23, :string
    add_column :registrations, :valid_24, :string
    add_column :registrations, :valid_25, :string
    add_column :registrations, :valid_26, :string
    add_column :registrations, :valid_27, :string
    add_column :registrations, :valid_28, :string
    add_column :registrations, :valid_46, :string
    add_column :registrations, :valid_47, :string
    add_column :registrations, :valid_48, :string
    add_column :registrations, :valid_49, :string
  end
end
