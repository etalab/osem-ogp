class AddBadgeField < ActiveRecord::Migration
  def change
    add_column :registrations, :badge, :string
  end
end
