class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.string :code
      t.string :label_en
      t.string :label_fr

      t.timestamps null: false
    end
  end
end
