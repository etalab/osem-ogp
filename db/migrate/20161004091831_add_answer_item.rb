class AddAnswerItem < ActiveRecord::Migration
  def change
    answer = Answer.find_or_create_by!(title: 'Subnational sessions - December 9')
    q = Question.find_by_id(8)
    q.answers << answer
    q.save!
  end
end
