class AddCountryCode < ActiveRecord::Migration
  def change
    add_column :registrations, :country_code, :string
  end
end
