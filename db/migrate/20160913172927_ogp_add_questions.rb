#Add question through migration because of the creation bug (first step)
class OgpAddQuestions < ActiveRecord::Migration
  def change
    qtype_yesno = QuestionType.find_or_create_by!(title: 'Yes/No')
    qtype_single = QuestionType.find_or_create_by!(title: 'Single Choice')
    qtype_multiple = QuestionType.find_or_create_by!(title: 'Multiple Choice')

    answer_yes = Answer.find_or_create_by!(title: 'Yes')
    answer_no = Answer.find_or_create_by!(title: 'No')

    answer_male = Answer.find_or_create_by!(title: 'Male')
    answer_female = Answer.find_or_create_by!(title: 'Female')
    answer_other = Answer.find_or_create_by!(title: 'Other')
    answer_notanswer = Answer.find_or_create_by!(title: 'Prefer not to answer')

    q = Question.find_or_initialize_by(title: 'Gender', question_type_id: qtype_single.id, global: true)
    q.answers = [answer_male, answer_female, answer_other, answer_notanswer]
    q.save!
  end
end
