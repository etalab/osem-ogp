class OgpSummitPhoneNumber < ActiveRecord::Migration
  def change
    add_column :events, :contact_phone_number, :text
  end
end
