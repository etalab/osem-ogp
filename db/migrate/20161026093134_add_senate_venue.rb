class AddSenateVenue < ActiveRecord::Migration
  def change
    q = Question.find_by_id(8)
    q.answers << Answer.find_or_create_by!(title: 'Open Parliament - December 8 (afternoon)')
  end
end
