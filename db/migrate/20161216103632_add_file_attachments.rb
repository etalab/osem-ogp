class AddFileAttachments < ActiveRecord::Migration
  def change
    add_attachment :events, :resource1
    add_attachment :events, :resource2
    add_attachment :events, :resource3
  end
end
