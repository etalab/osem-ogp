#Add question through because of the creation bug (second step)
class OgpAddQuestions2 < ActiveRecord::Migration
  def change
    qtype_single = QuestionType.find_or_create_by!(title: 'Single Choice')
    qtype_multiple = QuestionType.find_or_create_by!(title: 'Multiple Choice')
    
    answer_t1 = Answer.find_or_create_by!(title: 'Academia/ Student')
    answer_t2 = Answer.find_or_create_by!(title: 'Civil Society')
    answer_t3 = Answer.find_or_create_by!(title: 'Government')
    answer_t4 = Answer.find_or_create_by!(title: 'International Organization/ Multilateral')
    answer_t5 = Answer.find_or_create_by!(title: 'Media')
    answer_t6 = Answer.find_or_create_by!(title: 'Private Sector')
    answer_t7 = Answer.find_or_create_by!(title: 'Think Tank')
    answer_other = Answer.find_or_create_by!(title: 'Other')
    
    q = Question.find_or_initialize_by(title: 'Type of Organization', question_type_id: qtype_single.id, global: true)
    q.answers = [answer_t1, answer_t2, answer_t3, answer_t4, answer_t5, answer_t6, answer_t7, answer_other]
    q.save!
    
    answer_e1 = Answer.find_or_create_by!(title: 'Civil Society Morning - December 7')
    answer_e2 = Answer.find_or_create_by!(title: 'Opening plenary - December 7')
    answer_e3 = Answer.find_or_create_by!(title: 'Panels and workshops - December 8 and 9')
    answer_e4 = Answer.find_or_create_by!(title: 'Democracy Night - December 8')
    answer_e5 = Answer.find_or_create_by!(title: 'Toolbox hackathon - December 8 and 9')
    answer_e6 = Answer.find_or_create_by!(title: 'Closing plenary - December 9')
    
    q2 = Question.find_or_initialize_by(title: 'Which events would you like to participate in?', question_type_id: qtype_multiple.id, global: true)
    q2.answers = [answer_e1, answer_e2, answer_e3, answer_e4, answer_e5, answer_e6]
    q2.save!
    
    answer_l1 = Answer.find_or_create_by!(title: 'Opening cocktail - December 7')
    answer_l2 = Answer.find_or_create_by!(title: 'Lunch - December 8')
    answer_l3 = Answer.find_or_create_by!(title: 'Dinner - December 8')
    answer_l4 = Answer.find_or_create_by!(title: 'Lunch - December 9')
    
    q3 = Question.find_or_initialize_by(title: 'Will you eat on-site during the Summit?', question_type_id: qtype_multiple.id, global: true)
    q3.answers = [answer_l1, answer_l2, answer_l3, answer_l4]
    q3.save!
  end
end
