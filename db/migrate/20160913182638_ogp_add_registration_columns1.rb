class OgpAddRegistrationColumns1 < ActiveRecord::Migration
  def change
    add_column :registrations, :last_name, :string
    add_column :registrations, :first_name, :string
    add_column :registrations, :title, :string
    add_column :registrations, :organization_name, :string
    add_column :registrations, :telephone, :string
    add_column :registrations, :twitter, :string
    add_column :registrations, :country_residence, :string
    add_column :registrations, :nationality, :string
    add_column :registrations, :birth_date, :string
    add_column :registrations, :birth_place, :string
    add_column :registrations, :photo_file_name, :string
    add_column :registrations, :photo_content_type, :string
    add_column :registrations, :photo_file_size, :integer
    add_column :registrations, :photo_updated_at, :datetime
    add_column :registrations, :biography, :text
    add_column :registrations, :support_motivation, :text
  end
end
