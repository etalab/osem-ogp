class AddAnswerItems < ActiveRecord::Migration
  def change
    q = Question.find_by_id(8)
    q.answers << Answer.find_or_create_by!(title: 'Open Parliament - December 8')
    q.answers << Answer.find_or_create_by!(title: 'Panels and workshops - December 9')
    q.save!
  end
end
