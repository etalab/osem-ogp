class ChangeBirthDateType < ActiveRecord::Migration
  def change
    remove_column :registrations, :birth_date
    add_column :registrations, :birth_date, :datetime
  end
end
