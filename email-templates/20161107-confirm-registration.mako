## -*- coding: utf-8 -*-


From: ${from_email}
To: ${to_email}
Subject: ${"[OGP Summit] {}, Confirm your registration / Confirmez votre inscription".format(first_name or last_name or '') | qp}
MIME-Version: 1.0
Content-Type: text/plain; charset="${encoding}"

Bonjour ${full_name},

[Note: An english version of this email is available below.]

Nous avons bien reçu votre demande d’inscription au Sommet Mondial du  Partenariat pour un Gouvernement Ouvert et nous vous en remercions. Afin de valider votre demande nous vous demandons de CONFIRMER VOTRE PARTICIPATION AVANT LE 14 NOVEMBRE en suivant les instructions ci-dessous.

Le programme a été mis à jour et est disponible à l’adresse suivante : https://fr.ogpsummit.org/osem/conference/ogp-summit/schedule. Sur cette page, vous pouvez cliquer sur chaque bloc du programme pour visualiser le détail de chaque séquence. Ce programme peut être soumis à des futures modifications.

IMPORTANT : si vous ne validez pas votre inscription avant le 14 novembre, ou qu’elle est incomplète, votre inscription ne sera pas retenue et vous ne pourrez être officiellement accrédité au Sommet.

Afin de pouvoir valider définitivement votre inscription, suivez les étapes suivantes avant le 14 novembre :

1. Inscription : merci de vous rendre dans votre espace inscription <https://fr.ogpsummit.org/osem/conference/ogp-summit/register/confirmcheck> pour confirmer et compléter votre formulaire ou pour annuler votre inscription. Votre annulation pourra permettre à d’autres personnes intéressées par le Sommet d’y participer.

2. Photo d’identité : merci de fournir une photo d’identité en éditant le formulaire sur votre espace d’inscription, si vous ne l’avez pas encore fait. Une photo d’identité est nécessaire pour qu’on puisse vous remettre un badge d’accès répondant aux exigences de sécurité des différents sites.

3. Merci de confirmer les sequences auxquelles vous êtes sûr(e) de pouvoir assister en éditant le formulaire sur votre espace d’inscription. Le programme détaillé est dorénavant disponible sur le site du Sommet.

    Cette information nous permettra de gérer les flux pour répondre au mieux aux attentes de tous les participants. Nous avons reçu plus de 3000 inscriptions mais pour des raisons de sécurité liées à la capacité des salles, nous ne pourrons garantir l’accès de tous les participants à l’ensemble des séquences.

4. Si vous organisez une ou plusieurs session(s) : Vous pouvez modifier le titre et la description de votre proposition originelle sur votre espace personnel jusqu’au 16 novembre (date limite pour le programme imprimé). Vous pourrez rajouter et modifier les intervenants jusqu’au début du Sommet.

5. Matinée de la société civile : La Matinée de la Société Civile (7 décembre 2016) est réservée aux représentants des organisations de la société civile et quelques membres du monde de la recherche. Les inscriptions pourront être soumises à des contraintes de capacité des salles, veuillez confirmer votre présence au Sommet et à la matinée société civile compte tenu de ces informations et contraintes. Un mail séparé vous sera adressé avec le programme et autres informations importantes pour cet événement.


Nous vous remercions par avance,

Cordialement,

L’équipe PGO 2016.


----- English version -----


Dear  ${full_name},

We received your registration submission to the Open Government Partnership Global Summit and we thank you for that. To validate your request, we ask you to CONFIRM YOUR REGISTRATION BEFORE NOVEMBER 14TH by following the steps below.

The provisional agenda is now updated and available online <https://en.ogpsummit.org/osem/conference/ogp-summit/schedule>, you can now find the date, time, and room assigned to each session. Please note that this agenda is still subject to change.

IMPORTANT: if you don’t confirm your registration before November 14th, or if it is not complete, your registration will not be valid and you will not have an official accreditation for the Summit.

In order to confirm and validate your registration, please follow the next steps before November 14th:

1. Registration: please sign in your account <https://en.ogpsummit.org/osem/conference/ogp-summit/register/confirmcheck> to confirm and complete your form or to cancel your registration in case you will not be able to assist. Your cancellation can grant the possibility to participate to other interested people.

2. ID photo: Please provide us with an ID photo by editing your registration form in your account. An ID photo is necessary to be able to generate you an access badge in accordance to the security standards of all the venues of the Summit.

3. Please confirm the tracks to which you are certain to attend by editing your registration form in your account. The detailed agenda is now available on the Summit’s website.

    This information will let us manage the flows for each session and respect the participants’ choices as much as we can. We received more than 3000 registrations but for security reasons linked to the venues’ capacity, we will not be able to grant access to all the tracks to all the participants.

4. Note for session organizers: If you are a session organizer, you will be able to edit your original proposal’s title and description on ogpsummit.org until November 16th when that information will go to print. You will be able to add and edit speakers up until the Summit.

5. Civil Society morning: Please note that Civil Society Morning (December 7, 2016) is only open to representatives of civil society organizations and a few members of academia. Attendance may be capped due to venue capacity. Please confirm your attendance for the summit and Civil Society Morning  - as instructed in this email - with these in mind. You will receive another email with the agenda and other important information for this event in the coming weeks.


We will contact you shortly to share with you more information on the logistics. In the meantime, don’t hesitate to visit our FAQ <https://en.ogpsummit.org/faq/> and our practical information section <https://en.ogpsummit.org/travel-information/>.

Thank you in advance,

Kind regards.

The OGP 2016 team.