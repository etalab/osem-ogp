## -*- coding: utf-8 -*-

From: ${from_email}
To: ${to_email}
Subject: ${"OGP Hackathon: Come experiment the OGP Toolbox! | Contribuez à la Boîte à outils du gouvernement ouvert !" | qp}
MIME-Version: 1.0
Content-Type: text/plain; charset="${encoding}"

Dear ${first_name or full_name},

[Message en Français plus bas]

% if confirmation_date is None:
First thing: it's critical that you CONFIRM YOUR REGISTRATION NOW to the OGP Global Summit.
https://ogpsummit.org/osem/conference/ogp-summit/register/confirmcheck
IMPORTANT: if not confirmed before November 14th or if incomplete, your registration will not be valid and you will not have an official accreditation for the Summit.

We're now less than one month away!
% else:
We first want to thank you for confirming your registration to the OGP Global Summit. We're now less than one month away from the OGP Summit!
% endif

We're thrilled to have you on board for the OGP Toolbox Hackathon where you'll be able to find, use and share the best tools of the #opengov world: https://ogpsummit.org/the-open-government-toolbox/

The OGP Toolbox we're launching at the Summit is an international contributive platform that includes examples of use cases and technical criteria in order to learn from the experience of organizations that have already implemented open government tools.

Get started and make the most of the Hackathon! =>>>>> https://hackathon.ogpsummit.org/
1. Add a project and get people on board to tackle a real challenge
(following this template: https://hackathon.ogpsummit.org/projects/58204bf5ea3393203facf654)
2. Join a project to commit to its success
3. Follow a project to support it

The goal is to foster new projects and code reuse by cross-pollinating developers and hacktivists with policy makers, elected representatives, civil society members and citizens.

People from all around the world are coming to the OGP Summit with the same state of mind... let's open government!

But wait there's more... The Summit program is now online:
https://ogpsummit.org/osem/conference/ogp-summit/schedule

Take a look at the amazing schedule of Panels, Workshops and Pitches:
https://ogpsummit.org/osem/conference/ogp-summit/completeschedule

See you next month in Paris!

The OGP Summit Team

#OGPtoolbox #OGP16


----- Version française - French version -----


Bonjour ${first_name or full_name},

% if confirmation_date is None:
Tout d'abord : veuillez CONFIRMER IMPÉRATIVEMENT VOTRE INSCRIPTION MAINTENANT au Sommet mondial de l'OGP.
https://ogpsummit.org/osem/conference/ogp-summit/register/confirmcheck
IMPORTANT : si vous ne validez pas votre inscription avant le 14 novembre, ou si elle est incomplète, votre inscription ne sera pas retenue et vous ne serez pas officiellement accrédité au Sommet.

Nous sommes maintenant à moins d'un mois !
% else:
Tout d'abord, nous voulons vous remercier d'avoir bien confirmé votre inscription au Sommet mondial de l'OGP. Nous sommes maintenant à moins d'un mois du sommet de l'OGP !
% endif

Nous avons hâte de vous accueillir lors du Hackathon Toolbox où vous pourrez découvrir, utiliser et partager les meilleurs outils dans le monde du #GouvernementOuvert : https://ogpsummit.org/the-open-government-toolbox/

La Boîte à outils qui sera lancée à l'occasion du Sommet est une plateforme contributive internationale présentant des exemples concrets d'utilisation et des critères pour tirer profit de l'expérience d'organisations qui ont déjà implémenté des outils pour le gouvernement ouvert.

Lancez-vous et profitez au maximum de ce Hackathon ! =>>>>> https://hackathon.ogpsummit.org/
1. Ajoutez un projet et embarquez des contributeurs pour relever un défi concret
2. Rejoignez un projet pour participer à sa réussite
3. Suivez un projet pour le soutenir

Le but est d'encourager de nouvelles initiatives et la réutilisation de code, en faisant collaborer développeurs et hacktivistes avec des décideurs, élus, membres de la société civile et citoyens.

Des participants du monde entier ont répondu à l'appel et viendront au Sommet dans le même but... d'ouvrir le gouvernement!

C'est pas fini... Le programme du Sommet est maintenant en ligne :
https://ogpsummit.org/osem/conference/ogp-summit/schedule

Voici le détail des Conférences, Ateliers et Pitchs :
https://ogpsummit.org/osem/conference/ogp-summit/completeschedule

Rendez-vous le mois prochain à Paris !

L'équipe du Sommet OGP

#OGPtoolbox #OGP16