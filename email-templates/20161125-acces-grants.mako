## -*- coding: utf-8 -*-

From: ${from_email}
To: ${to_email}
Subject: ${"#OGP2016 - CONFIRMATION AND BADGES" | qp}
MIME-Version: 1.0
Content-Type: text/plain; charset="${encoding}"

Dear ${full_name},

[Message en Français plus bas]

We are pleased to confirm your participation at the OGP Global Summit 2016. It is great to see such interest in the Global Summit and we are looking forward to welcoming you in Paris.

In order to give the most number of people access to the Summit and due to the limited capacity of the rooms and security measures, it is possible that some of your registration choices may not be fully satisfied.

${first_name or full_name}, your badge will grant you access to the following tracks, subject to the limit of venue capacities at any given time and unless separately specified:

% if valid_23:
--> December 7, Civil Society Morning - Hôtel du Collectionneur. More information:  www.goo.gl/F5x8Uq
    (If you confirmed to attend CS morning, you would have received a separate email detailing the program and access information.)

% endif
% if valid_24:
--> December 7, Opening plenary - Salle Pleyel or streaming at Hôtel du Collectionneur. More information: www.goo.gl/xywYv0
    (A dedicated email will be sent with specific information on the access conditions.)

% endif
% if valid_25:
--> December 8, Panels and workshops - Palais d’Iéna and Palais de Tokyo. More information: www.goo.gl/WVBUhK
    - Democracy Night: www.goo.gl/obuRIl
    - Hackathon Toolbox: www.goo.gl/R36nrp

% endif
% if valid_47:
--> December 8,  Open Parliament at the National Assembly -Assemblée Nationale. More information: www.goo.gl/nnQ4yN

% endif
% if valid_49:
--> December 8, Open Parliament at the Senate – Sénat. More information: www.goo.gl/q29cML

% endif
% if valid_48:
--> December 9, Panels and workshops - Palais d’Iéna and Palais de Tokyo. More information:  www.goo.gl/UF3zXP

% endif
% if valid_46:
--> December 9, Subnational sessions - Hôtel de Ville de Paris. More information: www.goo.gl/vKgQ02

% endif
% if valid_28:
--> December 9, Closing Ceremony - Hôtel de Ville de Paris and streaming at Palais d’Iéna. More information: www.goo.gl/jp57xA

% endif
Due to aforementioned capacity and security measures, it is not possible to grant everyone access to all preferred tracks and venues. If you registered for certain tracks which are not listed above, please note that it has not been possible to grant you access to those.

VENUE ACCESS AND BADGES:

1. A personalised badge will be required to access the Summit venues. We strongly recommend that you pick up your badge before the beginning of the Summit at Hôtel du Collectionneur (51 - 57, rue de Courcelles 75008 Paris - France) on December 6 from 9 AM to 7 PM or December 7 from 7 AM to 7 PM.
   * Please note, you will need a government-issued photo ID to pick up your badge.
   * Do not forget to upload an ID photo to your registration form.
   * Visit our FAQ section for more information on accreditation : www.en.ogpsummit.org/faq.


2. You will find the detailed agenda information about each of the venues on the Summit website: www.ogpsummit.org

We will continue to communicate with you on other important information before the Summit. In the meantime, please visit our FAQ: www.en.ogpsummit.org/faq page or contact us at: summit@opengovpartnership.org.

Kind regards,

The OGP 2016 Summit team


 ----- Version française - French version -----


${full_name},

Nous sommes heureux de confirmer votre participation au Sommet Mondial du Partenariat pour un Gouvernement Ouvert 2016.

Nous avons tâché de répondre au mieux aux très nombreuses demandes d’inscription qui nous sont parvenues. Nous avons choisi de privilégier l’accès du plus grand nombre au Sommet, c’est pourquoi il se peut que tous vos choix ne soient pas pleinement satisfaits du fait de la capacité limitée des salles et des contraintes de sécurité auxquelles un sommet de ce type doit répondre.

${first_name or full_name}, votre badge vous permettra d’accéder aux séquences suivantes, dans la limite des places disponibles lors de votre arrivée :

% if valid_23:
--> 7 décembre - Matinée de la Société Civile, Hôtel du Collectionneur . Plus d'informations:  www.goo.gl/F5x8Uq
        (Un mail dédié vous sera envoyé précisant les conditions d’accès et le contenu du programme.)

% endif
% if valid_24:
--> 7 décembre - Plénière d’Ouverture, Salle Pleyel ou rediffusion à l’Hôtel du Collectionneur. Plus d'informations: www.goo.gl/xywYv0
        (Un mail dédié vous sera envoyé précisant les conditions d’accès et horaires)

% endif
% if valid_25:
--> 8 décembre - Conférences et Ateliers, Palais d‘Iéna et Palais de Tokyo. Plus d'informations: www.goo.gl/WVBUhK
        - Nuit de la Démocratie: www.goo.gl/obuRIl
        - Hackathon Toolbox: www.goo.gl/R36nrp

% endif
% if valid_47:
--> 8 décembre - Parlement Ouvert à l’Assemblée Nationale, Assemblée Nationale. Plus d'informations: www.goo.gl/nnQ4yN

% endif
% if valid_49:
--> 8 décembre - Parlement Ouvert au Sénat, Sénat. Plus d'informations: www.goo.gl/q29cML

% endif
% if valid_48:
--> 9 décembre - Conférences et Ateliers, Palais d’Iéna et Palais de Tokyo. Plus d'informations: www.goo.gl/UF3zXP

% endif
% if valid_46:
--> 9 décembre - Gouvernement Ouvert et Territoires, Hôtel de Ville. Plus d'informations: www.goo.gl/vKgQ02

% endif
% if valid_28:
--> 9 décembre – Clôture, Hôtel de Ville et retransmission au Palais d’Iéna. Plus d'informations: www.goo.gl/jp57xA

% endif
 Si une séquence que vous avez sélectionnée n’apparaît pas dans la liste ci-dessus, c’est qu’en raison du très grand nombre de demandes d’inscription, nous n’avons pas pu donner suite à l’intégralité de vos souhaits.

 ACCES AU SOMMET ET BADGES

 1. Pour éviter des temps d’attente trop importants, nous vous demandons de retirer en avance votre badge à l’Hôtel du Collectionneur (51 – 57, rue de Courcelles 75008 Paris – France) : le 6 décembre de  9 :00 à 19 :00 ou le 7 décembre de 7 :00 à 19 :00
    * Pensez à vous munir d’une pièce d’identité, indispensable pour vous délivrer un badge.
    * Vous devez également ajouter une photo d’identité dans votre espace d’inscription, laquelle doit figurer sur votre badge.
    * Consultez notre section FAQ pour plus d'informations sur l'accreditation : www.fr.ogpsummit.org/faq.


2. Toutes les informations pratiques sur chaque lieu et séquence ainsi que le détail du programme sont disponibles sur le site: www.ogpsummit.org


Cordialement,

L’Equipe du Sommet PGO
