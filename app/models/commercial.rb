class Commercial < ActiveRecord::Base
  require 'oembed'

  belongs_to :commercialable, polymorphic: true

  validates :url, presence: true, if: ":image.nil?"
  validates :url, format: URI::regexp(%w(http https)), if: ":image.nil?"

  validate :valid_url, if: ":image.nil?"

  has_attached_file :image, styles: { thumb: '100x100>', large: '300x300>' }, unless: ":image.nil?"

  validates_attachment_content_type :image,
                                    content_type: [/jpg/, /jpeg/, /png/, /gif/],
                                    size: { in: 0..1024.kilobytes },
                                    unless: ":image.nil?"

  def self.render_from_url(url)
    register_provider
    begin
      resource = OEmbed::Providers.get(url, maxwidth: 560, maxheight: 315)
      { html: resource.html.html_safe }
    rescue StandardError => exception
      { error: exception.message }
    end
  end

  private

  def valid_url
    result = Commercial.render_from_url(url)
    if result[:error]
      errors.add(:base, result[:error])
    end
  end

  def self.register_provider
    speakerdeck = OEmbed::Provider.new('http://speakerdeck.com/oembed.json')
    speakerdeck << 'https://speakerdeck.com/*'
    speakerdeck << 'http://speakerdeck.com/*'

    OEmbed::Providers.register(
        OEmbed::Providers::Youtube,
        OEmbed::Providers::Vimeo,
        OEmbed::Providers::Slideshare,
        OEmbed::Providers::Flickr,
        OEmbed::Providers::Instagram,
        speakerdeck
    )
  end
end
