module CriblageExport
  
  def CriblageExport.run_export
    xls_export_path = CONFIG["xls_export_path"]
    if xls_export_path.nil? or xls_export_path.length == 0
      return
    end
    existing = IO.readlines(xls_export_path + 'criblage-iena.tsv')
    existing_set = Set.new
    existing.each do |value|
      value = value.strip()
      existing_set << value
    end
    ogpsummit_conference = Conference.find_by(short_title: 'ogp-summit')
    CriblageExport.export_criblage(xls_export_path, ogpsummit_conference, existing_set)
  end
  
  def CriblageExport.checkString(s)
    s = s.strip()
    s = I18n.transliterate(s)
    s = s.upcase()
    s = s.sub(/\?/, "'")
    return s
  end
  
  def CriblageExport.checkFirstName(s)
    ignore_array = ["REV", "DR.", "AHM", "SHER", "Y."]
    s = s.strip()
    s = I18n.transliterate(s)
    s = s.upcase()
    split1 = s.split(",")
    s = split1[0]
    split2 = s.split(" ")
    index = 0;
    if split2.length > 1
      first = split2[0]
      if not ignore_array.index(first).nil?
        index = 1
      end
    end
    s = split2[index]
    return s
  end
  
  def CriblageExport.export_criblage(xls_export_path, ogpsummit_conference, existing_set)
    ogpsummit_registrations = ogpsummit_conference.registrations.where("criblage IS NULL").order('registrations.created_at ASC')
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(name: 'registration') do |sheet|
        countries = Hash.new(nil)
        Country.all.each do |country|
          countries[country.code] = country.label_fr;
        end
        bold_style = p.workbook.styles.add_style(b: true)
        row = []
        row << 'NOM'
        row << 'NOM DE JEUNE FILLE'
        row << 'PRÉNOM'
        row << 'DATE DE NAISSANCE'
        row << 'LIEU DE NAISSANCE'
        row << "DOCUMENT D’IDENTITÉ"
        row << 'N° DU DOCUMENT'
        row << 'NATIONALITÉ'

        sheet.add_row row, :style => bold_style

        Time::DATE_FORMATS[:birth_date] = '%d/%m/%Y'
        
        ogpsummit_registrations.each do |registration|
          last_name = CriblageExport.checkString(registration.last_name)
          first_name = CriblageExport.checkFirstName(registration.first_name)
          key = last_name + "\t" + first_name
          if not existing_set.include? key
            row = []
            row << last_name
            row << ''
            row << first_name
            if registration.birth_date?
              row << registration.birth_date.to_s(:birth_date)
            else
              row << ''
            end
            if registration.birthcountry_code?
              country = countries[registration.birthcountry_code]
              if country.nil?
                row << registration.birthcountry_code
              else
                row << CriblageExport.checkString(country)
              end
            else
              row << ''
            end
            row << ''
            row << ''
            if registration.nationality_code?
              country = countries[registration.nationality_code]
              if country.nil?
                row << registration.nationality_code
              else
                row << CriblageExport.checkString(country)
              end
            else
              row << CriblageExport.checkString(registration.nationality)
            end
            
            #row << registration.birth_place
          
            sheet.add_row row
          end
        end
      end
      p.serialize(xls_export_path + 'criblage.xlsx')
    end
  end

end