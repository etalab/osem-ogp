module XlsExport
  
  def XlsExport.run_export
    xls_export_path = CONFIG["xls_export_path"]
    if xls_export_path.nil? or xls_export_path.length == 0
      return
    end
    ogpsummit_conference = Conference.find_by(short_title: 'ogp-summit')
    XlsExport.export_registrations(xls_export_path, ogpsummit_conference)
    XlsExport.export_events(xls_export_path, ogpsummit_conference)
  end
  
  def XlsExport.export_registrations(xls_export_path, ogpsummit_conference)
    ogpsummit_registrations = ogpsummit_conference.registrations.includes(:user).order('registrations.created_at ASC')
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(name: 'registrations') do |sheet|
        answer_titles = Hash.new('?')
        ogpsummit_conference.questions.each do |question|
          question.qanswers.each do |qanswer|
            answer_titles[qanswer.id] = qanswer.answer.title
          end
        end
        countries = Hash.new(nil)
        Country.all.each do |country|
          countries[country.code] = country.label_fr;
        end
        q_event = ogpsummit_conference.questions.find_by_id(8)
        q_eat = ogpsummit_conference.questions.find_by_id(9)
        bold_style = p.workbook.styles.add_style(b: true)
        row = []
        row << 'ID'
        row << 'Badge'
        row << 'Création'
        row << 'Mise à jour'
        row << 'Confirmé'
        row << "Nom d'inscription"
        row << 'Courriel'
        row << 'Nom'
        row << 'Prénom'
        row << 'Titre'
        row << 'Genre'
        row << "Type de l'organisation"
        row << "Nom de l'organisation"
        row << "Téléphone"
        row << 'Twitter'
        row << 'Code pays'
        row << 'Pays de résidence'
        row << 'Ancien champ Nationalité'
        row << 'Code Nationalité'
        row << 'Nationalité'
        row << "Date de naissance"
        row << "Code Pays de naissance"
        row << "Pays de naissance"
        row << "Lieu de naissance"
        row << 'Type part.'
        row << 'Registration ID'
        q_event.qanswers.each do |qanswer|
          title = answer_titles[qanswer.id]
          row << title
          row << 'valid_' + qanswer.id.to_s + ': ' + title
        end  
        q_eat.qanswers.each do |qanswer|
          row << answer_titles[qanswer.id]
        end 
        row << 'Aide fin.'
        row << 'Motivation'
        row << 'Affichage public'
        row << 'Abo. OGP summit'
        row << 'Abo. OGP'
        row << 'Lettre Visa'
        row << 'Biographie'
        

        sheet.add_row row, :style => bold_style
        
        question_proc = Proc.new do |rw, question_map, id|
          text = ''
          if not question_map[id].nil?
            question_map[id].each do |qa_id|
              if text.length > 0
                  text = text + ', '
              end
              text = text + answer_titles[qa_id]
            end
          end
          rw << text
        end

        Time::DATE_FORMATS[:birth_date] = '%Y-%m-%d'
        
        ogpsummit_registrations.each do |registration|
          question_map = Hash.new(nil)
          qanswer_map = Hash.new(nil)
          attributes = registration.attributes
          registration.qanswers.each do |qa|
            q_array = question_map[qa.question_id]
            if q_array.nil?
              q_array = Array.new
              question_map[qa.question_id] = q_array
            end
            q_array << qa.id
            qanswer_map[qa.id] = 1
          end
          
          row = []
          row << registration.user_id
          row << registration.badge
          row << registration.created_at
          row << registration.updated_at
          row << registration.confirmation_date
          row << registration.name
          row << registration.email
          row << registration.last_name
          row << registration.first_name
          row << registration.title
          question_proc.call(row, question_map, 6)
          question_proc.call(row, question_map, 7)
          row << registration.organization_name
          row << registration.telephone
          row << registration.twitter
          if registration.country_code?
            row << registration.country_code
            country = countries[registration.country_code]
            if country.nil?
              row << registration.country_code
            else
              row << country
            end
          else
            row << ''
            row << ''
          end
          row << registration.nationality
          if registration.nationality_code?
            row << registration.nationality_code
            country = countries[registration.nationality_code]
            if country.nil?
              row << registration.nationality_code
            else
              row << country
            end
          else
            row << ''
            row << ''
          end
          if registration.birth_date?
            row << registration.birth_date.to_s(:birth_date)
          else
            row << ''
          end
          if registration.birthcountry_code?
            row << registration.birthcountry_code
            country = countries[registration.birthcountry_code]
            if country.nil?
              row << registration.birthcountry_code
            else
              row << country
            end
          else
            row << ''
            row << ''
          end
          row << registration.birth_place
          question_proc.call(row, question_map, 15)
          row << registration.id
          q_event.qanswers.each do |qanswer|
            if qanswer_map.has_key?(qanswer.id)
              row << 'X'
            else
              row << ''
            end
            validation = attributes["valid_" + qanswer.id.to_s]
            if validation.nil?
              row << ''
            else
              row << validation
            end
          end  
          q_eat.qanswers.each do |qanswer|
            if qanswer_map.has_key?(qanswer.id)
              row << 'X'
            else
              row << ''
            end
          end 
          question_proc.call(row, question_map, 10)
          row << registration.support_motivation
          question_proc.call(row, question_map, 11)
          question_proc.call(row, question_map, 12)
          question_proc.call(row, question_map, 13)
          question_proc.call(row, question_map, 14)
          row << registration.biography
          sheet.add_row row
        end
      end
      p.serialize(xls_export_path + 'registrations.xlsx')
    end
  end
  
  def XlsExport.export_events(xls_export_path, ogpsummit_conference)
    ogpsummit_events = ogpsummit_conference.program.events
    event_type_titles = Hash.new('?')
    EventType.all.each do |event_type|
      event_type_titles[event_type.id] = event_type.title
    end
    track_titles = Hash.new('?')
    Track.all.each do |track|
      track_titles[track.id] = track.name;
    end
    room_titles = Hash.new('?')
    Room.all.each do |room|
      room_titles[room.id] = room.name
    end
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(name: 'registrations') do |sheet|
        bold_style = p.workbook.styles.add_style(b: true)
        row = []
        row << 'ID'
        row << 'Status'
        row << 'Titre'
        row << 'Sous-titre'
        row << 'Proposé par'
        row << 'Intervenants'
        row << 'Préinscription'
        row << 'Type'
        row << 'Lieu'
        row << 'Date'
        row << 'Thème'
        row << 'Langue'
        row << 'Besoins matériels'
        row << 'Résumé'
        sheet.add_row row, :style => bold_style
      
        ogpsummit_events.each do |event|
          event_submitter = event.submitter
          row = []
          row << event.id
          row << event.state
          row << event.title
          row << event.subtitle
          row << event.submitter_names
          row << event.speaker_names
          if event.require_registration
            row << 'X'
          else
            row << ''
          end
          if event.event_type_id.nil?
            row << ''
          else
            row << event_type_titles[event.event_type_id]
          end
          if event.room_id.nil?
            row << ''
          else
            row << room_titles[event.room_id]
          end
          if event.start_time.nil?
            row << ''
          else
            row << event.start_time.strftime("%A %d %H:%M")
          end
          if event.track_id.nil?
            row << ''
          else
            row << track_titles[event.track_id]
          end
          if event.language.nil?
            row << ''
          else
            row << event.language
          end
          row << event.description
          row << event.abstract
          sheet.add_row row
        end
      end
      p.serialize(xls_export_path + 'events.xlsx')
    end
  end

end