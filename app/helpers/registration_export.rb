module RegistrationExport
  
  def RegistrationExport.run_export
    registration_export_path = CONFIG["registration_export_path"]
    if registration_export_path.nil? or registration_export_path.length == 0
      return
    end
    ogpsummit_conference = Conference.find_by(short_title: 'ogp-summit')
    export_csv(registration_export_path, ogpsummit_conference)
    export_photo(registration_export_path, ogpsummit_conference)
  end
  
  def RegistrationExport.export_csv(registration_export_path, ogpsummit_conference)
    ogpsummit_registrations = ogpsummit_conference.registrations.includes(:user).order('registrations.created_at ASC')
    answer_titles = Hash.new('?')
    ogpsummit_conference.questions.each do |question|
      question.qanswers.each do |qanswer|
        answer_titles[qanswer.id] = qanswer.answer.title
      end
    end
    countries = Hash.new(nil)
    Country.all.each do |country|
      countries[country.code] = country.label_fr;
    end
    question_proc = Proc.new do |rw, question_map, id|
        text = ''
        if not question_map[id].nil?
          question_map[id].each do |qa_id|
            if text.length > 0
                text = text + ', '
            end
            text = text + answer_titles[qa_id]
          end
        end
        rw << text
      end
    q_event = ogpsummit_conference.questions.find_by_id(8)
    q_eat = ogpsummit_conference.questions.find_by_id(9)
    CSV.open(registration_export_path + "registration.csv", "wb") do |csv|
      row = [] 
      row << 'ID'
      row << 'Nom'
      row << 'Prénom'
      row << 'Titre'
      row << 'Genre'
      row << "Type de l'organisation"
      row << "Nom de l'organisation"
      row << 'Code pays'
      row << 'Pays de résidence'
      row << 'Type du participant'
      q_event.qanswers.each do |qanswer|
        row << answer_titles[qanswer.id]
      end  
      q_eat.qanswers.each do |qanswer|
        row << answer_titles[qanswer.id]
      end 
      row << 'Aide financière'
      row << 'Création'
      row << 'Mise à jour'
      row << 'Confirmé'
      csv << row
      ogpsummit_registrations.each do |registration|
        question_map = Hash.new(nil)
        qanswer_map = Hash.new(nil)
        attributes = registration.attributes
        registration.qanswers.each do |qa|
          q_array = question_map[qa.question_id]
          if q_array.nil?
            q_array = Array.new
            question_map[qa.question_id] = q_array
          end
          q_array << qa.id
          qanswer_map[qa.id] = 1
        end
        row = []
        row << registration.user_id
        row << registration.last_name
        row << registration.first_name
        row << registration.title
        question_proc.call(row, question_map, 6)
        question_proc.call(row, question_map, 7)
        row << registration.organization_name
        if registration.country_code?
          row << registration.country_code
          country = countries[registration.country_code]
          if country.nil?
            row << registration.country_code
          else
            row << country
          end
        else
          row << ''
          row << ''
        end
        question_proc.call(row, question_map, 15)
        q_event.qanswers.each do |qanswer|
          validation = attributes["valid_" + qanswer.id.to_s]
          if validation.nil?
            row << 0
          else
            row << 1
          end
        end  
        q_eat.qanswers.each do |qanswer|
          if qanswer_map.has_key?(qanswer.id)
            row << 1
          else
            row << 0
          end
        end
        if not question_map[10].nil?
          question_map[10].each do |qa_id|
            if answer_titles[qa_id] == 'Yes'
              row << 1
            else
              row << 0
            end
          end
        else
          row << 0
        end
        row << registration.created_at
        row << registration.updated_at
        row << registration.confirmation_date
        csv << row
      end
    end
  end
  
  def RegistrationExport.export_photo(registration_export_path, ogpsummit_conference)
    ogpsummit_registrations = ogpsummit_conference.registrations.includes(:user).order('registrations.created_at ASC')
    ogpsummit_registrations.each do |registration|
      user_id = registration.user_id
      if !registration.photo.nil?
        path = registration.photo.path
        if path.nil?
          puts "path.nil: " + registration.user_id.to_s
        elsif !File.file?(path)
          puts "!File.file?: " + registration.user_id.to_s + " / " + path
        else
          idx = path.rindex('.')
          extension = path[idx..(path.length)].downcase
          if extension == '.jpeg'
            extension = '.jpg'
          end
          destination_path = registration_export_path + "photos/photo-" + user_id.to_s + '.jpg'
          if extension == '.png'
            system("convert '" + path + "' " + destination_path)
           else 
            FileUtils.cp(path, destination_path)
          end
        end
      else
        puts "registration.photo.nil: " + registration.user_id.to_s
      end
    end
    
  end
end