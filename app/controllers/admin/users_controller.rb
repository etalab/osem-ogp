module Admin
  class UsersController < Admin::BaseController
    load_and_authorize_resource

    def new
      @user = User.new
    end

    def index
      @users = User.all
    end

    def show
      # Variable @show_attributes holds the attributes that are visible for the 'show' action
      # If you want to change the attributes that are shown in the 'show' action of users
      # add/remove the attributes in the following string array
      @show_attributes = %w(name email username nickname affiliation biography registered attended roles created_at
                            updated_at sign_in_count current_sign_in_at last_sign_in_at
                            current_sign_in_ip last_sign_in_ip)
    end

    def update
      if params[:newpassword]
        newpassword = params[:newpassword].strip
        if newpassword.length > 0
          @user.password = newpassword
          @user.save!
        end
      end
      if params[:confirmuser]
        if params[:confirmuser] == "yes"
          @user.confirm
        end
      end
      if params[:confirmregistration]
        if params[:confirmregistration] == "yes"
          registration = @user.registrations.first
          if !registration.nil?
            registration.confirmation_date = Time.now
            registration.save!
          end
        end
      end
      if @user.update_attributes(user_params)
       redirect_to edit_admin_user_path(@user),
            notice: "Updated #{@user.username} (#{@user.name} / #{@user.email})!"
      else
       flash[:error] = "Could not update #{@user.username} (#{@user.name} / #{@user.email}). #{@user.errors.full_messages.join('. ')}."
        render action: 'edit'
      end
    end

    def edit
    
    end

    private

    def user_params
      params.require(:user).permit(:email, :name, :email_public, :biography, :nickname, :affiliation, :is_admin, :username, :login, :is_disabled,
                                   :tshirt, :mobile, :volunteer_experience, :languages, :avatar, :remove_avatar, role_ids: [])
    end
  end
end
