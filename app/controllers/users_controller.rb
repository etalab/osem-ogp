class UsersController < ApplicationController
  load_and_authorize_resource

  # GET /users/1
  def show
    @speaker_events = @user.events.where('state = ? AND event_users.event_role=?', 'confirmed', 'speaker')
    @registration = @user.registrations.first
    @is_public = false
    if !@registration.nil?
      public_answer = @registration.qanswers.where(id: 35).first
      if !public_answer.nil? and !@registration.last_name.nil? and @registration.last_name.length > 0
        @is_public = true
      end
    end
    @countries = Hash.new(nil)
    Country.all.each do |country|
      if @hostnamelang == 'fr'
        @countries[country.code] = country.label_fr
      else
        @countries[country.code] = country.label_en
      end
    end
  end

  # GET /users/1/edit
  def edit
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      redirect_to @user, notice: 'User was successfully updated.'
    else
      flash[:error] = "An error prohibited your Profile from being saved: #{@user.errors.full_messages.join('. ')}."
      render :edit
    end
  end

  private

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:name, :biography, :nickname, :affiliation, :avatar, :remove_avatar)
    end
end
