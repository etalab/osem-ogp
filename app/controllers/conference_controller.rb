class ConferenceController < ApplicationController
  protect_from_forgery with: :null_session
  before_filter :respond_to_options
  load_and_authorize_resource find_by: :short_title
  load_resource :program, through: :conference, singleton: true, except: :index

  def index
    @current = Conference.where('end_date >= ?', Date.current).order('start_date ASC')
    @antiquated = @conferences - @current
  end

  def show
    @remaining = 7 - Date.current.day
  end


  def new_proposals
    @events = @conference.program.events.new_proposals
  end

  def schedule
    @rooms = @conference.venue.rooms if @conference.venue
    @events = @conference.program.events
    @dates = @conference.start_date..@conference.end_date

    if @dates == Date.current
      @today = Date.current.strftime('%Y-%m-%d')
    else
      @today = @conference.start_date.strftime('%Y-%m-%d')
    end
  end
  
  def completeschedule
    @rooms = @conference.venue.rooms.order(:order).where("id < 14") if @conference.venue
    @events = @conference.program.events.confirmed
    @dates = @conference.start_date..@conference.end_date

    @schedule_dates = Date.new(2016, 12, 8)..Date.new(2016, 12, 9)
    
    if @dates == Date.current
      @today = Date.current.strftime('%Y-%m-%d')
    else
      @today = @conference.start_date.strftime('%Y-%m-%d')
    end
  end
  
  def subnationalschedule
    @rooms = @conference.venue.rooms.order(:order).where("id > 13") if @conference.venue
    @events = @conference.program.events.confirmed
    @dates = @conference.start_date..@conference.end_date

    @schedule_dates = Date.new(2016, 12, 9)..Date.new(2016, 12, 9)
    
    if @dates == Date.current
      @today = Date.current.strftime('%Y-%m-%d')
    else
      @today = Date.new(2016, 12, 9).strftime('%Y-%m-%d')
    end
  end

  def eventlists
    @rooms = @conference.venue.rooms.order(:order) if @conference.venue
    @events = @conference.program.events
    @dates = @conference.start_date..@conference.end_date

    @schedule_dates = Date.new(2016, 12, 8)..Date.new(2016, 12, 9)
    
    if @dates == Date.current
      @today = Date.current.strftime('%Y-%m-%d')
    else
      @today = @conference.start_date.strftime('%Y-%m-%d')
    end
  end
  
  def attendeelist
    @countries = Hash.new(nil)
    Country.all.each do |country|
      if @hostnamelang == 'fr'
        @countries[country.code] = country.label_fr
      else
        @countries[country.code] = country.label_en
      end
    end
    @public_registrations = @conference.registrations.includes(:user).where("valid_23 IS NOT NULL OR valid_24 IS NOT NULL OR valid_25 IS NOT NULL OR valid_26 IS NOT NULL OR valid_27 IS NOT NULL OR valid_28 IS NOT NULL OR valid_46 IS NOT NULL OR valid_47 IS NOT NULL OR valid_48 IS NOT NULL OR valid_49 IS NOT NULL").order('last_name ASC')
    @letters = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
  end
  
  def gallery_photos
    @photos = @conference.photos
    render 'photos', formats: [:js]
  end

  private

  def respond_to_options
    respond_to do |format|
      format.html { head :ok }
    end if request.options?
  end
end
