class ConferenceRegistrationsController < ApplicationController
  before_filter :authenticate_user!, except: [:new, :create, :confirmcheck]
  load_resource :conference, find_by: :short_title
  authorize_resource :conference_registrations, class: Registration, except: [:new, :create]
  before_action :set_registration, only: [:edit, :update, :destroy, :show, :confirmupdate]

  def new
    redirect_to root_path, error: "Sorry, registration is closed"
  end

  def show
    @workshops = @registration.workshops
    @total_price = Ticket.total_price(@conference, current_user)
    @tickets = current_user.ticket_purchases.where(conference_id: @conference.id)
  end
  
  def confirmcheck
    if current_user && @conference.user_registered?(current_user)
      @registration = Registration.find_by(conference: @conference, user: current_user)
    end
    render "confirmation"
  end
  
  def confirmupdate
    @registration.confirmation_date = Time.now
    @registration.save!
    redirect_to edit_conference_conference_registrations_path(@conference.short_title),
        notice: 'Your registration is confirmed'
  end

  def edit

  end

  def create
    redirect_to root_path, error: "Sorry, registration is closed"
  end

  def update
    if @registration.update_attributes(registration_params)
      redirect_to  conference_conference_registrations_path(@conference.short_title),
                   notice: 'Registration was successfully updated.'
    else
      flash[:error] = "Could not update your registration for #{@conference.title}: "\
                        "#{@registration.errors.full_messages.join('. ')}."
      render :edit
    end
  end
  
  def confirmation
    
  end

  def destroy
    if @registration.destroy
      redirect_to root_path,
                  notice: "You are not registered for #{@conference.title} anymore!"
    else
      redirect_to conference_conference_registrations_path(@conference.short_title),
                  error: "Could not delete your registration for #{@conference.title}: "\
                  "#{@registration.errors.full_messages.join('. ')}."
    end
  end

  protected

  def set_registration
    @registration = Registration.find_by(conference: @conference, user: current_user)
    if !@registration
      redirect_to new_conference_conference_registrations_path(@conference.short_title),
                  error: "Can't find a registration for #{@conference.title} for you. Please register."
    end
  end

  def user_params
    params.require(:user).permit(:username, :email, :name, :password, :password_confirmation)
  end

  def registration_params
    params.require(:registration).
        permit(
          :conference_id, :arrival, :departure,
          :last_name, :first_name, :title, :organization_name, :telephone, :twitter, :country_residence, :country_code, :nationality_code,
          :birthcountry_code, :birth_date, :birth_place, :biography, :support_motivation,
          :photo, :remove_photo,
          :volunteer,
          vchoice_ids: [], qanswer_ids: [],
          qanswers_attributes: [],
          event_ids: [],
          user_attributes: [
            :username, :email, :name, :password, :password_confirmation]
    )
  end
end
